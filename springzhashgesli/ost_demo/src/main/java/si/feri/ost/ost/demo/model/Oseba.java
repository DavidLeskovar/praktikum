package si.feri.ost.ost.demo.model;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Oseba{

    private int id;
    private String ime;
    private String priimek;
    private String upIme;
    private String geslo;
    private String ePostniNaslov;
    private int idTipUporabnika;
    private static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public String getePostniNaslov() {
        return ePostniNaslov;
    }

    public void setePostniNaslov(String ePostniNaslov) {
        this.ePostniNaslov = ePostniNaslov;
    }

    public int getIdTipUporabnika() {
        return idTipUporabnika;
    }

    public void setIdTipUporabnika(int idTipUporabnika) {
        this.idTipUporabnika = idTipUporabnika;
    }

    // Obvezen prazen konstruktor!
    public Oseba() {
    }

    // Ne nastavljamo id nikoli sami!
    public Oseba(String ime, String priimek) {
        this.ime = ime;
        this.priimek = priimek;
    }

    public Oseba(int id, String upIme) {
        this.id = id;
        this.upIme = upIme;
    }

    public Oseba(int id, String upIme, String geslo) {
        this.id = id;
        this.upIme = upIme;
        this.geslo= passwordEncoder.encode(geslo);
    }

    public Oseba(int id, String upIme, String geslo, String ePostniNaslov) {
        this.id = id;
        this.upIme = upIme;
        this.geslo = passwordEncoder.encode(geslo);
        this.ePostniNaslov = ePostniNaslov;
    }

    public Oseba(int id, String ime, String priimek, String upIme, String geslo, String ePostniNaslov) {
        this.id = id;
        this.ime = ime;
        this.priimek = priimek;
        this.upIme = upIme;
        this.geslo = geslo;
        this.ePostniNaslov = ePostniNaslov;
    }

    public Oseba(String ime, String priimek, String upIme, String geslo, String ePostniNaslov, int idTipUporabnika){
        this.ime = ime;
        this.priimek = priimek;
        this.upIme=upIme;
        this.geslo= passwordEncoder.encode(geslo);
        this.ePostniNaslov=ePostniNaslov;
        this.idTipUporabnika=idTipUporabnika;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getUpIme() {
        return upIme;
    }

    public void setUpIme(String upIme) {
        this.upIme = upIme;
    }

    public String getGeslo() {
        return geslo;
    }

    public void setGeslo(String geslo) {
        this.geslo = passwordEncoder.encode(geslo);
    }

    public void setEPostniNaslov(String ePostniNaslov) {
        this.ePostniNaslov = ePostniNaslov;
    }

    public String getEPostniNaslov() {
        return ePostniNaslov;
    }

    public boolean pravilnoGeslo(String vpisanoGeslo){
        return passwordEncoder.matches(vpisanoGeslo, this.geslo);
    }
}
