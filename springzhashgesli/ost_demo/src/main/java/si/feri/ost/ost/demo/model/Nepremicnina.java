package si.feri.ost.ost.demo.model;

import javax.persistence.Lob;
import java.util.Base64;

public class Nepremicnina {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipNepremicnine_id() {
        return tipNepremicnine_id;
    }

    public void setTipNepremicnine_id(int tipNepremicnine_id) {
        this.tipNepremicnine_id = tipNepremicnine_id;
    }

    private int id;
    private String naziv;
    private int velikost;
    private int stsostanovalcev;
    private int cenanajema;
    private int cenamesstroskov;
    private String opis;
    private int tipNepremicnine_id;
    private int drzava_id;
    private int mesto_id;
    private int okolica_id;
    private int uporabnik_id;
    private int tiplastnika_id;
    private int regija_id;
    @Lob
    private byte[] slika;
    private int naslov_id;
    private String naslov;
    private String ePostniNaslovLastnika;

    public String getePostniNaslovLastnika() {
        return ePostniNaslovLastnika;
    }

    public void setePostniNaslovLastnika(String ePostniNaslovLastnika) {
        this.ePostniNaslovLastnika = ePostniNaslovLastnika;
    }

    public int getNaslov_id() {
        return naslov_id;
    }

    public void setNaslov_id(int naslov_id) {
        this.naslov_id = naslov_id;
    }


    public Nepremicnina(int id, String naziv, int cenanajema, int cenamesstroskov, String opis) {
        this.id = id;
        this.naziv = naziv;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
    }


    public Nepremicnina(int id, String naziv, int cenanajema, int cenamesstroskov, String opis, byte[] slika) {
        this.id = id;
        this.naziv = naziv;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
        this.slika = slika;
    }

    public Nepremicnina(int id, String naziv, int velikost, int stsostanovalcev, int cenanajema, int cenamesstroskov, String opis, byte[] slika, String naslov) {
        this.id = id;
        this.naziv = naziv;
        this.velikost = velikost;
        this.stsostanovalcev = stsostanovalcev;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
        this.slika = slika;
        this.naslov = naslov;
    }

    public Nepremicnina(String naziv, int velikost, int stsostanovalcev, int cenanajema, int cenamesstroskov, String opis, int tipNepremicnine_id, int drzava_id, int mesto_id, int okolica_id, int uporabnik_id, int tiplastnika_id, int regija_id, byte[] slika, int naslov_id) {
        this.naziv = naziv;
        this.velikost = velikost;
        this.stsostanovalcev = stsostanovalcev;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
        this.tipNepremicnine_id = tipNepremicnine_id;
        this.drzava_id = drzava_id;
        this.mesto_id = mesto_id;
        this.okolica_id = okolica_id;
        this.uporabnik_id = uporabnik_id;
        this.tiplastnika_id = tiplastnika_id;
        this.regija_id = regija_id;
        this.slika = slika;
        this.naslov_id = naslov_id;
    }

    public Nepremicnina(String naziv, int velikost, int stsostanovalcev, int cenanajema, int cenamesstroskov, String opis, int tipNepremicnine_id, int drzava_id, int mesto_id, int okolica_id, int uporabnik_id, int tiplastnika_id, int regija_id, byte[] slika) {
        this.naziv = naziv;
        this.velikost = velikost;
        this.stsostanovalcev = stsostanovalcev;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
        this.tipNepremicnine_id = tipNepremicnine_id;
        this.drzava_id = drzava_id;
        this.mesto_id = mesto_id;
        this.okolica_id = okolica_id;
        this.uporabnik_id = uporabnik_id;
        this.tiplastnika_id = tiplastnika_id;
        this.regija_id = regija_id;
        this.slika = slika;
    }

    public Nepremicnina(String naziv, int velikost, int stsostanovalcev, int cenanajema, int cenamesstroskov, String opis, int tipNepremicnine_id, int drzava_id, int mesto_id, int okolica_id, int uporabnik_id, int tiplastnika_id) {
        this.naziv = naziv;
        this.velikost = velikost;
        this.stsostanovalcev = stsostanovalcev;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
        this.tipNepremicnine_id = tipNepremicnine_id;
        this.drzava_id = drzava_id;
        this.mesto_id = mesto_id;
        this.okolica_id = okolica_id;
        this.uporabnik_id = uporabnik_id;
        this.tiplastnika_id = tiplastnika_id;
    }

    public Nepremicnina(int id, String naziv, String opis, byte[] slika) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.slika = slika;
    }

    public Nepremicnina(String naziv, int velikost, int stsostanovalcev, int cenanajema, int cenamesstroskov, String opis, int tipNepremicnine_id, int drzava_id, int mesto_id, int okolica_id, int uporabnik_id, int tiplastnika_id, int regija_id) {
        this.naziv = naziv;
        this.velikost = velikost;
        this.stsostanovalcev = stsostanovalcev;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
        this.tipNepremicnine_id = tipNepremicnine_id;
        this.drzava_id = drzava_id;
        this.mesto_id = mesto_id;
        this.okolica_id = okolica_id;
        this.uporabnik_id = uporabnik_id;
        this.tiplastnika_id = tiplastnika_id;
        this.regija_id = regija_id;
    }

    public Nepremicnina(int id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public Nepremicnina() {
    }

    public Nepremicnina(int id, String naziv, int tipNepremicnine_id) {
        this.id = id;
        this.naziv = naziv;
        this.tipNepremicnine_id = tipNepremicnine_id;
    }

    public Nepremicnina(String naziv, int velikost, int stsostanovalcev, int cenanajema, int cenamesstroskov, String opis) {
        this.naziv = naziv;
        this.velikost = velikost;
        this.stsostanovalcev = stsostanovalcev;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getVelikost() {
        return velikost;
    }

    public void setVelikost(int velikost) {
        this.velikost = velikost;
    }

    public int getStsostanovalcev() {
        return stsostanovalcev;
    }

    public void setStsostanovalcev(int stsostanovalcev) {
        this.stsostanovalcev = stsostanovalcev;
    }

    public int getCenanajema() {
        return cenanajema;
    }

    public void setCenanajema(int cenanajema) {
        this.cenanajema = cenanajema;
    }

    public int getCenamesstroskov() {
        return cenamesstroskov;
    }

    public void setCenamesstroskov(int cenamesstroskov) {
        this.cenamesstroskov = cenamesstroskov;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public int getDrzava_id() {
        return drzava_id;
    }

    public void setDrzava_id(int drzava_id) {
        this.drzava_id = drzava_id;
    }

    public int getMesto_id() {
        return mesto_id;
    }

    public void setMesto_id(int mesto_id) {
        this.mesto_id = mesto_id;
    }

    public int getOkolica_id() {
        return okolica_id;
    }

    public void setOkolica_id(int okolica_id) {
        this.okolica_id = okolica_id;
    }

    public int getUporabnik_id() {
        return uporabnik_id;
    }

    public void setUporabnik_id(int uporabnik_id) {
        this.uporabnik_id = uporabnik_id;
    }

    public int getTiplastnika_id() {
        return tiplastnika_id;
    }

    public void setTiplastnika_id(int tiplastnika_id) {
        this.tiplastnika_id = tiplastnika_id;
    }

    public int getRegija_id() {
        return regija_id;
    }

    public void setRegija_id(int regija_id) {
        this.regija_id = regija_id;
    }

    public byte[] getSlika() {
        return slika;
    }

    public void setSlika(byte[] slika) {
        this.slika = slika;
    }

    public Nepremicnina(int id, String naziv, byte[] slika) {
        this.id = id;
        this.naziv = naziv;
        this.slika = slika;
    }

    public String returnSlika() {
        if (this.slika != null) {
            return new String(Base64.getEncoder().encode(this.slika));
        } else {
            return "";
        }
    }

    public Nepremicnina(int id, String naziv, String opis, byte[] slika, int naslov_id) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.slika = slika;
        this.naslov_id = naslov_id;
    }

    public Nepremicnina(String naziv, int velikost, int stsostanovalcev, int cenanajema, int cenamesstroskov, String opis, int tipNepremicnine_id, int drzava_id, int mesto_id, int okolica_id, int uporabnik_id, int tiplastnika_id, int regija_id, byte[] slika, int naslov_id, String ePostniNaslovLastnika) {
        this.naziv = naziv;
        this.velikost = velikost;
        this.stsostanovalcev = stsostanovalcev;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
        this.tipNepremicnine_id = tipNepremicnine_id;
        this.drzava_id = drzava_id;
        this.mesto_id = mesto_id;
        this.okolica_id = okolica_id;
        this.uporabnik_id = uporabnik_id;
        this.tiplastnika_id = tiplastnika_id;
        this.regija_id = regija_id;
        this.slika = slika;
        this.naslov_id = naslov_id;
        this.ePostniNaslovLastnika = ePostniNaslovLastnika;
    }

    public Nepremicnina(int id, String naziv, int tipNepremicnine_id, String ePostniNaslovLastnika) {
        this.id = id;
        this.naziv = naziv;
        this.tipNepremicnine_id = tipNepremicnine_id;
        this.ePostniNaslovLastnika = ePostniNaslovLastnika;
    }

    public Nepremicnina(int id, String naziv, String opis, byte[] slika, String ePostniNaslovLastnika) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.slika = slika;
        this.ePostniNaslovLastnika = ePostniNaslovLastnika;

    }

    public Nepremicnina(int id, String naziv, int velikost, int stsostanovalcev, int cenanajema, int cenamesstroskov, String opis, byte[] slika, String naslov, String ePostniNaslovLastnika) {
        this.id = id;
        this.naziv = naziv;
        this.velikost = velikost;
        this.stsostanovalcev = stsostanovalcev;
        this.cenanajema = cenanajema;
        this.cenamesstroskov = cenamesstroskov;
        this.opis = opis;
        this.slika = slika;
        this.naslov = naslov;
        this.ePostniNaslovLastnika = ePostniNaslovLastnika;
    }
}
