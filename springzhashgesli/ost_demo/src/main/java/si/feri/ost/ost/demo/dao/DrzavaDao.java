package si.feri.ost.ost.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.model.Drzava;
import si.feri.ost.ost.demo.model.Oseba;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class DrzavaDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Drzava> getAllDrzave() {
        String sql = "SELECT * FROM Drzava"; //sql query

        List<Drzava> ret = new ArrayList<Drzava>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            String naziv = (String) row.get("Naziv");
            int id = (int) row.get("id");



            ret.add(new Drzava(id,naziv));
        }
        return ret;
    }



    public int addDrzava(String naziv) {
        String sql = "INSERT into Drzava(naziv) values(?)";

        return jdbcTemplate.update(sql, new Object[]{naziv});
    }


    public int getIdDrzave(String naziv){




       List<Drzava> list = getAllDrzave();

        for (Drzava drz: list) {
            if(drz.getNaziv().equalsIgnoreCase(naziv)){
                return list.indexOf(drz)+1;
            }
        }
        return 0;







    }



    public int obstaja(String naziv1){
        List<Drzava> drzave =getAllDrzave();
        for (Drzava drz: drzave) {
            if(drz.getNaziv().equalsIgnoreCase(naziv1)){
                return drz.getId();
            }
        }
        return 0;
    }
}
