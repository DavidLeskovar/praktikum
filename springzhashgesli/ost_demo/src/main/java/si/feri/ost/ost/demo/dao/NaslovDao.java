package si.feri.ost.ost.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.model.Drzava;
import si.feri.ost.ost.demo.model.Naslov;
import si.feri.ost.ost.demo.model.Oseba;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class NaslovDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Naslov> getAllNaslovi() {
        String sql = "SELECT * FROM Naslov"; //sql query

        List<Naslov> ret = new ArrayList<Naslov>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            String naziv = (String) row.get("Naziv");
            int id = (int) row.get("id");



            ret.add(new Naslov(id,naziv));
        }
        return ret;
    }



    public int addNaslov(String naziv) {
        String sql = "INSERT into Naslov(naziv) values(?)";

        return jdbcTemplate.update(sql, new Object[]{naziv});
    }



    public int obstaja(String naziv1){
        List<Naslov> naslovi =getAllNaslovi();
        for (Naslov nas: naslovi) {
            if(nas.getNaziv().equalsIgnoreCase(naziv1)){
                return nas.getId();
            }
        }
        return 0;
    }
}