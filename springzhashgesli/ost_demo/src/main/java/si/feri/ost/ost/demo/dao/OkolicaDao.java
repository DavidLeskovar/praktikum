package si.feri.ost.ost.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.model.Drzava;
import si.feri.ost.ost.demo.model.Mesto;
import si.feri.ost.ost.demo.model.Okolica;
import si.feri.ost.ost.demo.model.Oseba;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class OkolicaDao {

    @Autowired
    JdbcTemplate jdbcTemplate;


    public List<Okolica> getAllOkolice() {
        String sql = "SELECT * FROM Okolica"; //sql query

        List<Okolica> ret = new ArrayList<Okolica>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            String naziv = (String) row.get("Naziv");
            int id = (int) row.get("id");



            ret.add(new Okolica(id,naziv));
        }
        return ret;
    }


    public int addOkolica(String naziv) {
        String sql = "INSERT into Okolica(naziv) values(?)";

        return jdbcTemplate.update(sql, new Object[]{naziv});
    }


    public int getIdOkolice(String naziv){


        List<Okolica> list = getAllOkolice();

        for (Okolica ok: list) {
            if(ok.getNaziv().equalsIgnoreCase(naziv)){
                return list.indexOf(ok)+1;
            }
        }
        return 0;







    }


    public int obstaja(String naziv1){
        List<Okolica> okolice =getAllOkolice();
        for (Okolica ok: okolice) {
            if(ok.getNaziv().equalsIgnoreCase(naziv1)){
                return ok.getId();
            }
        }
        return 0;
    }
}
