package si.feri.ost.ost.demo.dao;

import org.springframework.data.repository.CrudRepository;
import si.feri.ost.ost.demo.model.OsebaS;

import java.util.List;

public interface OsebaSDao extends CrudRepository<OsebaS, Long> {

    List<OsebaS> findByPriimek(String priimek);
}
