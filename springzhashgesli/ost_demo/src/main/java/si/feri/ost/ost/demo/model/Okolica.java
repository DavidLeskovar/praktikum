package si.feri.ost.ost.demo.model;

public class Okolica {

    private int id;
    private String naziv;

    public Okolica() {
    }


    public Okolica(String naziv) {
        this.naziv = naziv;
    }

    public Okolica(int id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
