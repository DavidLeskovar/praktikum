package si.feri.ost.ost.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import si.feri.ost.ost.demo.model.Nepremicnina;
import si.feri.ost.ost.demo.domain.User;
import si.feri.ost.ost.demo.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.web.multipart.MultipartFile;
import si.feri.ost.ost.demo.dao.*;
import si.feri.ost.ost.demo.model.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    OsebaDao osebaDao;
    @Autowired
    NepremicninaDao NepremicninaDao;
    @Autowired
    DrzavaDao DrzavaDao;
    @Autowired
    MestoDao MestoDao;
    @Autowired
    OkolicaDao OkolicaDao;
    @Autowired
    NaslovDao NaslovDao;

    private Logger logger = LoggerFactory.getLogger(Controller.class);

    @Autowired
    private NotificationService notificationService;

    @RequestMapping(value = { "/", "/home"}, method = RequestMethod.GET)
    public String home(Model model,HttpSession session) {

        boolean prijavljen = true;
        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
        if(trenutni==null){

            prijavljen=false;
            model.addAttribute("prijavljen", prijavljen);
            return "home";

        }

        String ime = trenutni.getUpIme();
        model.addAttribute("prijavljen", prijavljen);
        model.addAttribute("ime", ime);



        return "home";
    }

    @RequestMapping(value = {"/index"}, method = RequestMethod.GET)
    public String index(Model model) {

        model.addAttribute("osebe", osebaDao.getAllOsebe());
        return "dodajanje";
    }

    @RequestMapping(value = { "/prijava"}, method = RequestMethod.GET)
    public String prijava(Model model,HttpSession session) {


        boolean prijavljen = false;
        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
        if(trenutni!=null){


            prijavljen=true;
            String ime = trenutni.getUpIme();
            model.addAttribute("prijavljen", prijavljen);
            model.addAttribute("ime", ime);






            return "zeprijavljen";
        }

        return "prijava";
    }

    @RequestMapping(value = { "/dodajanje"}, method = RequestMethod.GET)
    public String registracija(Model model,HttpSession session) {


        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");

        boolean prijavljen = true;

        if(trenutni!=null){
            String ime = trenutni.getUpIme();
            model.addAttribute("ime", ime);
            model.addAttribute("prijavljen", prijavljen);
            return"zeprijavljen";
        }

        else{
            return "dodajanje";
        }




    }




    @RequestMapping(value = { "/view"}, method = RequestMethod.GET)
    public String view(Model model,HttpSession session) {


        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");

        boolean prijavljen = false;

        if(trenutni!=null){

            prijavljen = true;
            String ime = trenutni.getUpIme();
            model.addAttribute("ime", ime);

        }



        model.addAttribute("prijavljen", prijavljen);
        return "view";
    }


    @RequestMapping(value = { "/neuspesnaRegistracija"}, method = RequestMethod.GET)
    public String neuspesnaRegistracija(Model model) {

        return "neuspesnaRegistracija";
    }


    @RequestMapping(value = { "/iskanje"}, method = RequestMethod.GET)
    public String iskanje(Model model,HttpSession session) {

        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
        boolean prijavljen = false;


        if(trenutni!=null){
            prijavljen = true;
            String ime = trenutni.getUpIme();
            model.addAttribute("ime", ime);


        }


        model.addAttribute("prijavljen", prijavljen);
        return "iskanje2";
    }

    @RequestMapping(value = { "/dodajanjeNep"}, method = RequestMethod.GET)
    public String dodajanjeNep(Model model,HttpSession session) {

        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
        if(trenutni==null){
            return "potrebnaPrijava";
        }


        boolean prijavljen = true;
        String ime = trenutni.getUpIme();
        model.addAttribute("ime", ime);
        model.addAttribute("prijavljen", prijavljen);


        return "dodajanjeNep";
    }

    @RequestMapping(value = { "/seznamnepremicnin"}, method = RequestMethod.GET)
    public String seznamnepremicnin(Model model) {

        return "seznamnepremicnin";
    }

    @RequestMapping(value = { "/viewOwn"}, method = RequestMethod.GET)
    public String viewOwn(Model model,HttpSession session) {




        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
        if(trenutni==null){
            return "potrebnaPrijava";
        }
        int idOsebe = trenutni.getId();

        boolean prijavljen = true;
        String ime = trenutni.getUpIme();
        model.addAttribute("ime", ime);
        model.addAttribute("prijavljen", prijavljen);



        List<Nepremicnina> listNep = NepremicninaDao.getAllNepremicninePoOsebi(idOsebe);

        model.addAttribute("nepremicnine", listNep);


        return "viewOwn";
    }

    @RequestMapping(value = {"/addOseba"}, method = RequestMethod.POST)
    public String addOseba(Model model,
                             @RequestParam(value = "name") String ime,
                             @RequestParam(value = "lastname") String priimek,
                             @RequestParam(value = "username") String upIme,
                             @RequestParam(value = "password") String geslo,
                             @RequestParam(value = "ePostniNaslov") String ePostniNaslov
                             ) {



        List<Oseba> test = osebaDao.getAllOsebe();

        for (Oseba oseba:test) {

            if(oseba.getUpIme().equalsIgnoreCase(upIme)||oseba.getePostniNaslov().equalsIgnoreCase(ePostniNaslov)){


                model.addAttribute("osebe", osebaDao.getAllOsebe());


                return "neuspesnaRegistracija";
            }


        }

        osebaDao.addOseba(ime, priimek,upIme,geslo,ePostniNaslov);


        return "home";
    }

    @RequestMapping(value = {"/prijavaUporabnika"}, method = RequestMethod.POST)
    public String prijavaUporabnika(Model model, HttpSession session,

                                    @RequestParam(value = "username") String upIme,
                                    @RequestParam(value = "password") String geslo

    ) {






        List<Oseba> test = osebaDao.getAllOsebe();
        int idUporabnika;

        for (Oseba oseba:test) {

            if(oseba.getUpIme().equalsIgnoreCase(upIme)&&oseba.pravilnoGeslo(geslo)){


                boolean prijavljen = true;
                idUporabnika=oseba.getId();
                session.setAttribute("trenutniUporabnik", new Oseba(idUporabnika, upIme, geslo));
                Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
                String ime = trenutni.getUpIme();
                model.addAttribute("prijavljen", prijavljen);
                model.addAttribute("ime", ime);


                return "view";
            }


        }

        return "neuspesnaPrijava";



    }

    @RequestMapping(value = {"/addNepremicnina"}, method = RequestMethod.POST)
    public String dodajNepremicnino(Model model,HttpSession session,
                             @RequestParam(value = "name") String naziv,
                             @RequestParam(value = "velikost") String velikost,
                             @RequestParam(value = "stsostanovalcev") String stsostanovalcev,
                             @RequestParam(value = "cenanajema") String cenanajema,
                             @RequestParam(value = "cenamesstroskov") String cenamesstroskov,
                             @RequestParam(value = "opis") String opis,
                                    @RequestParam(value = "tipnepremicnine") String tipnepremicnine_id,
                                    @RequestParam(value = "drzava") String drzava,
                                    @RequestParam(value = "mesto") String mesto,
                                    @RequestParam(value = "okolica") String okolica,
                                    @RequestParam(value = "tiplastnika") String tiplastnika_id,
                                    @RequestParam(value = "regija") String regija_id,
                                    @RequestParam(value = "slika") MultipartFile file,
                                    @RequestParam(value = "naslov") String naslov


    ){

        int v1 = Integer.parseInt(velikost);
        int st1 = Integer.parseInt(stsostanovalcev);
        int c1 = Integer.parseInt(cenanajema);
        int cenam = Integer.parseInt(cenamesstroskov);
        int tip = Integer.parseInt(tipnepremicnine_id);
        int tip2 = Integer.parseInt(tiplastnika_id);
        int regija = Integer.parseInt(regija_id);



        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");

        String ePostniNaslovLastnika = (String) trenutni.getePostniNaslov();

        if(trenutni == null){

            return "neuspesnaPrijava";

        }

        int lastnik = trenutni.getId();

        int idDrzave;

        idDrzave=DrzavaDao.obstaja(drzava);
        if(idDrzave==0){
            DrzavaDao.addDrzava(drzava);
            idDrzave = DrzavaDao.obstaja(drzava);
        }



        int idMesta;

        idMesta=MestoDao.obstaja(mesto);
        if(idMesta==0){
            MestoDao.addMesto(mesto);
            idMesta = MestoDao.obstaja(mesto);
        }



        int idOkolice;

        idOkolice=OkolicaDao.obstaja(okolica);
        if(idOkolice==0){
            OkolicaDao.addOkolica(okolica);
            idOkolice = OkolicaDao.obstaja(okolica);
        }


        int idNaslova;

        idNaslova=NaslovDao.obstaja(naslov);
        if(idNaslova==0){
            NaslovDao.addNaslov(naslov);
            idNaslova = NaslovDao.obstaja(naslov);
        }






        try {
            byte[] bytes = file.getBytes();
            NepremicninaDao.addNepremicnina(naziv,v1,st1,c1,cenam,opis,tip,idDrzave,idMesta,idOkolice,lastnik,tip2,regija,bytes,idNaslova);
            model.addAttribute("nepremicnine", NepremicninaDao.getAllNepremicninePoOsebi1(lastnik));


            boolean prijavljen = true;
            String ime = trenutni.getUpIme();
            model.addAttribute("ime", ime);
            model.addAttribute("prijavljen", prijavljen);







            return "viewOwn";
        } catch (IOException e) {
            e.printStackTrace();
        }





        return "viewOwn";



     }
    @RequestMapping(value = {"/iskanje"}, method = RequestMethod.POST)
    public String iskanje(Model model,

                          @RequestParam(value = "tip") String tip

    ) {




        int tipnepremicnine = Integer.parseInt(tip);
        List<Nepremicnina> test = NepremicninaDao.getAllNepremicninePoTipu(tipnepremicnine);
        model.addAttribute("nepremicnine", test);











        return "iskanje2";




    }
    @RequestMapping(value = {"/iskanjepoceni"}, method = RequestMethod.POST)
    public String iskanjepoceni(Model model,

                                @RequestParam(value = "cenaOd") String cenaOd,
                                @RequestParam(value = "cenaDo") String cenaDo
    ) {


        int odCena = Integer.parseInt(cenaOd);
        int doCena = Integer.parseInt(cenaDo);

        List<Nepremicnina> test = NepremicninaDao.getAllNepremicninePoCeni(odCena,doCena);

        model.addAttribute("nepremicnine", test);

        return "iskanje";




    }

    @RequestMapping(value = {"/iskanjeporegiji"}, method = RequestMethod.POST)
    public String iskanjeporegiji(Model model,

                                @RequestParam(value = "regija") String regija

    ) {

        int reg = Integer.parseInt(regija);


        List<Nepremicnina> test = NepremicninaDao.getAllNepremicninePoRegiji(reg);

        model.addAttribute("nepremicnine", test);

        return "iskanje";




    }

    @RequestMapping(value = {"/iskanjepoparametrih"}, method = RequestMethod.POST)
    public String iskanjepoparametrih(Model model,HttpSession session,

                                      @RequestParam(value = "tipnepremicnine") String tipNep,
                                      @RequestParam(value = "regija") String regija,
                                      @RequestParam(value = "cenaOd") String cenaOd,
                                      @RequestParam(value = "cenaDo") String cenaDo

    ) {

        int reg = Integer.parseInt(regija);
        int tip = Integer.parseInt(tipNep);
        int cenaO = Integer.parseInt(cenaOd);
        int cenaD = Integer.parseInt(cenaDo);


        List<Nepremicnina> test = NepremicninaDao.getAllNepremicninePoPara(tip,reg,cenaO,cenaD);


        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
        boolean prijavljen = false;


        if(trenutni!=null){
            prijavljen = true;
            String ime = trenutni.getUpIme();
            model.addAttribute("ime", ime);


        }


        model.addAttribute("prijavljen", prijavljen);






        model.addAttribute("nepremicnine", test);

        return "seznamnepremicnin";




    }

    @RequestMapping(value = {"/odjava"}, method = RequestMethod.POST)
    public String odjava(Model model, HttpSession session



    ) {

        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
        if(trenutni==null){


            return "potrebnaPrijava";

        }





        session.invalidate();


        return "home";


    }


    @RequestMapping(value ={"/poslano"}, method = RequestMethod.POST)
    public String poslano(Model model,HttpSession session,

                          @RequestParam(value = "idNep") String idNep

    ){
        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");

        int idNepInt = Integer.parseInt(idNep);

        List<Nepremicnina> listNep = NepremicninaDao.getAllNepremicnina1(idNepInt);

        model.addAttribute("nepremicnine", listNep);

        User user = new User();

        for (Nepremicnina nep:listNep
             ) {
            String ePostniNaslovLastnika = nep.getePostniNaslovLastnika();

            user.setFirstName("janez");
            user.setLastName("novak");
            user.setEmailAddress(ePostniNaslovLastnika);
        }





        try {
            notificationService.sendNotification(user);
        }catch (MailException e ){
            //catch error
            logger.info("error sending email: " + e.getMessage());
        }

        return "poslano";
    }



    @RequestMapping(value ={"/map"}, method = RequestMethod.GET)
    public String map(Model model,HttpSession session){
        return "map";
    }



    @RequestMapping(value = {"/brisanje"}, method = RequestMethod.POST)
    public String brisanje(Model model,HttpSession session,

                          @RequestParam(value = "idNep") String idNep

    ) {

        int id = Integer.parseInt(idNep);

        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
        if(trenutni==null){
            return "potrebnaPrijava";
        }


        NepremicninaDao.deleteNepremicnina(id);

        int idOsebe = trenutni.getId();


        List<Nepremicnina> listNep = NepremicninaDao.getAllNepremicninePoOsebi(idOsebe);

        model.addAttribute("nepremicnine", listNep);

        boolean prijavljen = true;

        String ime = trenutni.getUpIme();
        model.addAttribute("ime", ime);


        model.addAttribute("prijavljen", prijavljen);








        return "viewOwn";







    }

    @RequestMapping(value = {"/podrobnosti"}, method = RequestMethod.POST)
    public String podrobnosti(Model model,HttpSession session,

                           @RequestParam(value = "idNep") String idNep

    ) {






        int idNepInt = Integer.parseInt(idNep);

        List<Nepremicnina> listNep = NepremicninaDao.getAllNepremicnina(idNepInt);

        model.addAttribute("nepremicnine", listNep);


        Oseba trenutni = (Oseba) session.getAttribute("trenutniUporabnik");
        boolean prijavljen = false;


        if(trenutni!=null){
            prijavljen = true;
            String ime = trenutni.getUpIme();
            model.addAttribute("ime", ime);


        }


        model.addAttribute("prijavljen", prijavljen);




        return "soba";

    }


    //@RequestMapping(value ={"/poslano"})
    //public String poslano(){
    //    notificationService.sendNotification(oseba);
    //}

}
