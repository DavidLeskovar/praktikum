package si.feri.ost.ost.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.model.Oseba;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class OsebaDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Oseba> getAllOsebe() {
        String sql = "SELECT * FROM Oseba"; //sql query

        List<Oseba> ret = new ArrayList<Oseba>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id     = (int) row.get("id");
            String ime     = (String) row.get("Ime");
            String priimek = (String) row.get("Priimek");
            String upIme = (String) row.get("upIme");
            String geslo = (String) row.get("geslo");
            String ePostniNaslov = (String) row.get("ePostniNaslov");
            String idTipUporabnika = (String) row.get("idTipUporabnika");




            ret.add(new Oseba(id,upIme,geslo,ePostniNaslov));
        }
        return ret;
    }

    public List<Oseba> getidOsebe(String uporabniskoime) {
        String sql = "SELECT * FROM Oseba where UPIME = " + uporabniskoime;

        List<Oseba> ret = new ArrayList<Oseba>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            String id = (String) row.get("id");
            String upIme = (String) row.get("upIme");


            int random = Integer.parseInt(id);


            ret.add(new Oseba(random,upIme));
        }
        return ret;
    }


    public Oseba getOseba(String ime, String priimek) {
        String sql = "SELECT id,ime,priimek FROM Oseba WHERE UPIME = ? and priimek = ?";
        Oseba os = (Oseba) jdbcTemplate.queryForObject(sql,
                                                       new Object[]{ime, priimek},
                                                       new BeanPropertyRowMapper(Oseba.class));
        return os;
    }

    public Oseba getOseba(int id) {
        String sql = "SELECT * FROM Oseba WHERE id = ?";
        Oseba o = (Oseba) jdbcTemplate.queryForObject(sql,
                                                      new Object[]{id},
                                                      new BeanPropertyRowMapper(Oseba.class));
        return o;
    }

    public int addOseba(String ime, String priimek) {
        String sql = "INSERT into Oseba(ime,priimek) values(?,?)";

        return jdbcTemplate.update(sql, new Object[]{ime, priimek});
    }

    public int addOseba(Oseba o) {
        String sql = "INSERT into Oseba(id,ime,priimek) values(?,?,?)";

        return jdbcTemplate.update(sql, new Object[]{o.getId(), o.getIme(), o.getPriimek()});
    }

    public int updateOseba(int id, String ime, String priimek) {
        String sql = "UPDATE Oseba set ime=?, priimek=? where id=?";

        return jdbcTemplate.update(sql, new Object[]{ime, priimek, id});
    }

    public int addOseba(String ime, String priimek, String upIme, String geslo, String ePostniNaslov) {
        String sql = "INSERT into Oseba(ime,priimek,upIme,geslo,ePostniNaslov) values(?,?,?,?,?)";

        return jdbcTemplate.update(sql, new Object[]{ime, priimek,upIme,geslo,ePostniNaslov});
    }


}
