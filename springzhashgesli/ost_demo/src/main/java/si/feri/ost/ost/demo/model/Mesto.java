package si.feri.ost.ost.demo.model;

public class Mesto {

    private int id;
    public String naziv;


    public Mesto() {
    }

    public Mesto(String naziv) {

        this.naziv = naziv;
    }

    public Mesto(int id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
