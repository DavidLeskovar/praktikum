package si.feri.ost.ost.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.model.Drzava;
import si.feri.ost.ost.demo.model.Mesto;
import si.feri.ost.ost.demo.model.Oseba;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class MestoDao {

    @Autowired
    JdbcTemplate jdbcTemplate;


    public List<Mesto> getAllMesta() {
        String sql = "SELECT * FROM Mesto"; //sql query

        List<Mesto> ret = new ArrayList<Mesto>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            String naziv = (String) row.get("Naziv");
            int id = (int) row.get("id");



            ret.add(new Mesto(id,naziv));
        }
        return ret;
    }



    public int addMesto(String naziv) {
        String sql = "INSERT into Mesto(naziv) values(?)";

        return jdbcTemplate.update(sql, new Object[]{naziv});
    }


    public int getIdMesta(String naziv){


        List<Mesto> list = getAllMesta();

        for (Mesto mes: list) {
            if(mes.naziv.equalsIgnoreCase(naziv)){
                return list.indexOf(mes)+1;
            }
        }
        return 0;







    }



    public int obstaja(String naziv1){
        List<Mesto> mesta =getAllMesta();
        for (Mesto mes: mesta) {
            if(mes.naziv.equalsIgnoreCase(naziv1)){
                return mes.getId();
            }
        }
        return 0;
    }
}
