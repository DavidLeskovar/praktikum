package si.feri.ost.ost.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.model.Nepremicnina;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class NepremicninaDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Nepremicnina> getAllNepremicnine() {
        String sql = "SELECT * FROM Nepremicnina"; //sql query

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            byte[] slika   = (byte[]) row.get("slika");




            ret.add(new Nepremicnina(id, naziv, slika));
        }
        return ret;
    }


    public List<Nepremicnina> getAllNepremicninePoOsebi(int oseba) {
        String sql = "SELECT * FROM Nepremicnina WHERE uporabnik_id ="+oseba; //sql query

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            byte[] slika   = (byte[]) row.get("slika");
            String opis = (String) row.get("opis");
            String ePostniNaslovLastnika = (String) row.get("ePostniNaslovUporabnika");





            ret.add(new Nepremicnina(id, naziv,opis, slika, ePostniNaslovLastnika));
        }
        return ret;
    }



    public int addNepremicnina(String naziv, int velikost, int stsostanovalcev, int cenanajema, int cenamesstroskov,String opis,int tipNepremicnine_id,int drzava_id,int mesto_id,int okolica_id,int uporabnik_id,int tiplastnika_id,int regija_id,byte[] slika,int naslov_id) {
        String sql = "INSERT into Nepremicnina(naziv,velikost,stsostanovalcev,cenanajema,cenastroskov,opis, tipNepremicnine_id,drzava_id,mesto_id,okolica_id,uporabnik_id,tiplastnika_id,regija_id,slika,naslov_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        return jdbcTemplate.update(sql, new Object[]{naziv, velikost,stsostanovalcev,cenanajema,cenamesstroskov,opis,tipNepremicnine_id,drzava_id,mesto_id,okolica_id,uporabnik_id,tiplastnika_id,regija_id,slika,naslov_id});
    }
    public List<Nepremicnina> getAllNepremicninePoTipu(int tip) {
        String sql = "SELECT * FROM Nepremicnina where tipnepremicnine_id = "+tip; //sql query

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            int tipnepremicnine = (int) row.get("TIPNEPREMICNINE_ID");



            ret.add(new Nepremicnina(id, naziv,tipnepremicnine));
        }
        return ret;
    }


    public List<Nepremicnina> getAllNepremicninePoCeni(int odCena,int doCena) {
        String sql = "SELECT * FROM Nepremicnina where cenanajema between "+odCena +"and "+doCena;

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            int tipnepremicnine = (int) row.get("TIPNEPREMICNINE_ID");



            ret.add(new Nepremicnina(id, naziv,tipnepremicnine));
        }
        return ret;
    }


    public List<Nepremicnina> getAllNepremicninePoRegiji(int regija) {
        String sql = "SELECT * FROM Nepremicnina where regija_id = "+regija;

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            int tipnepremicnine = (int) row.get("TIPNEPREMICNINE_ID");



            ret.add(new Nepremicnina(id, naziv,tipnepremicnine));
        }
        return ret;
    }

    public List<Nepremicnina> getAllNepremicninePoParametrih(int tip,int regija,int cenaOd, int cenaDo) {
        String sql = "SELECT * FROM NEPREMICNINA  where TIPNEPREMICNINE_ID = "+tip+" and REGIJA_ID = "+regija+" and CENANAJEMA between "+cenaOd+" and "+cenaDo;

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            byte[] slika   = (byte[]) row.get("slika");
            String opis = (String) row.get("opis");

            ret.add(new Nepremicnina(id, naziv,opis, slika));
        }
        return ret;
    }

    public List<Nepremicnina> getAllNepremicninePoPara(int tip,int regija,int cenaOd, int cenaDo) {
        String sql = "SELECT * FROM NEPREMICNINA  where TIPNEPREMICNINE_ID ="+tip;

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            byte[] slika   = (byte[]) row.get("slika");
            String opis = (String) row.get("opis");




            ret.add(new Nepremicnina(id, naziv,opis, slika));
        }
        return ret;
    }







    public List<Nepremicnina> getAllNepremicnina(int idNep) {
        String sql = "SELECT * FROM NEPREMICNINA INNER JOIN NASLOV ON NEPREMICNINA.NASLOV_ID=NASLOV.ID WHERE NEPREMICNINA.ID ="+idNep;

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            byte[] slika   = (byte[]) row.get("slika");
            String opis = (String) row.get("opis");
            int cena = (int) row.get("cenanajema");
            int stroski = (int) row.get("cenastroskov");
            int stevilo = (int) row.get("stsostanovalcev");
            int velikost = (int) row.get("velikost");
            String naslov = (String) row.get("naziv");


            ret.add(new Nepremicnina(id, naziv,velikost,stevilo,cena,stroski,opis, slika,naslov));




        }
        return ret;
    }

    public int deleteNepremicnina(int id){



        String sql = "DELETE FROM NEPREMICNINA WHERE ID ="+id;

        return jdbcTemplate.update(sql);

    }

    public List<Nepremicnina> getAllNepremicninePoOsebi1(int oseba) {
        String sql = "SELECT * FROM Nepremicnina INNER JOIN Oseba ON NEPREMICNINA.UPORABNIK_ID = OSEBA.ID WHERE uporabnik_id =" + oseba; //sql query

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            byte[] slika = (byte[]) row.get("slika");
            String opis = (String) row.get("opis");
            String ePostniNaslovLastnika = (String) row.get("EPOSTNINASLOV");


            ret.add(new Nepremicnina(id, naziv, opis, slika, ePostniNaslovLastnika));
        }
        return ret;
    }

    public List<Nepremicnina> getAllNepremicnina1(int idNep) {
        String sql = "SELECT * FROM NEPREMICNINA INNER JOIN NASLOV ON NEPREMICNINA.NASLOV_ID=NASLOV.ID WHERE NEPREMICNINA.ID ="+idNep;

        List<Nepremicnina> ret = new ArrayList<Nepremicnina>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {

            int id = (int) row.get("id");
            String naziv = (String) row.get("naziv");
            byte[] slika   = (byte[]) row.get("slika");
            String opis = (String) row.get("opis");
            int cena = (int) row.get("cenanajema");
            int stroski = (int) row.get("cenastroskov");
            int stevilo = (int) row.get("stsostanovalcev");
            int velikost = (int) row.get("velikost");
            String naslov = (String) row.get("naziv");
            String ePostniNaslovLastnika = (String) row.get("EPOSTNINASLOV");


            ret.add(new Nepremicnina(id, naziv,velikost,stevilo,cena,stroski,opis, slika,naslov, ePostniNaslovLastnika));




        }
        return ret;
    }
}
