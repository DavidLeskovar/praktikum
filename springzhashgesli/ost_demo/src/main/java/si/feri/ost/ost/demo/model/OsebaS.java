package si.feri.ost.ost.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OsebaS {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String ime;
    private String priimek;


    // Obvezen prazen konstruktor!
    public OsebaS() {
    }

    // Ne nastavljamo id nikoli sami!
    public OsebaS(String ime, String priimek) {
        this.ime = ime;
        this.priimek = priimek;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }
}
