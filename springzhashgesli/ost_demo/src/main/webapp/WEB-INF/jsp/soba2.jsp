<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>viewOthers</title>
</head>
<body style="background-color: #ccebff" onload="prijavljen()">
<script>
    function prijavljen(){
        var pri = ${trenutni};
        if (pri!=null){
            document.getElementById("prij").innerHTML = ${trenutni}
        }
    }
</script>
<div class="jumbotron text-center" style="margin-bottom:0; background-color:lightskyblue">
    <h1>Cimri</h1>
    <p>Preprosto najemanje in oddajanje sob!</p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="#">Meni</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home">Domaca Stran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="view">Ogled Sob</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="dodajanjeNep">Dodajanje Sob</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <h1>Seznam nepremicnin, ki ustrezajo vasim kriterijem: </h1>
    <table>
        <c:forEach items="${nepremicnine}" var="o">
            <tr>
                <div class="container" style="background-color: #b3e0ff; margin-bottom:1%">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">
                            <img src="data:image/png;base64,${o.returnSlika()}" class="float-right" style="margin:2%" height="250" width="500"/>
                            <h2>Naziv: ${o.naziv}</h2>
                            <p>Opis: ${o.opis}</p>
                            <p>Cena najema: ${o.cenanajema} Evrov</p>
                            <p>Cena najema: ${o.cenamesstroskov} Evrov</p>

                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                    <form action="/podrobnosti" method="post">
                        <button name="idNep" type="submit" value="${o.id}" style="margin-bottom:1%">Podrobnosti</button>
                    </form>
                </div>
            </tr>

        </c:forEach>



    </table>



</div>



</body>
<footer>
    <div class="jumbotron text-center" style="margin-bottom:0">
    </div>
</footer>
</html>