<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>soba</title>
</head>
<body style="background-color: #ccebff" onload="prijavljen()">

<c:if test="${prijavljen eq true}">


    <div class="row text-right" style="background-color:lightskyblue">
        <div class="col-9"></div>
        <div class="col-2 text-right">
            <p>Prijavljen uporabnik: ${ime}</p>
        </div>
        <div class="col-1 text-right">
            <form action="/odjava" method="post">

                <button type="submit">Odjava</button><br>

            </form>
        </div>
    </div>

</c:if>

<div class="jumbotron text-center" style="margin-bottom:0; background-color:lightskyblue">

    <h1>Cimri</h1>
    <p>Preprosto najemanje in oddajanje sob!</p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="#">Meni</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home">Domaca Stran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="view">Ogled Sob</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="dodajanjeNep">Dodajanje Sob</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <table>
        <c:forEach items="${nepremicnine}" var="o">
            <tr>
                <div class="container" style="background-color: #b3e0ff">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">


                            <img src="data:image/png;base64,${o.returnSlika()}" class="float-right" style="margin:2%" height="250" width="500"/>
                            <h2>Naslov: ${o.naziv}</h2>
                            <p>Opis: ${o.opis}</p>
                            <p>Cena najema: ${o.cenanajema} Evrov</p>
                            <p>Cena mesecnih stroskov: ${o.cenamesstroskov} Evrov</p>
                            <p>Velikost: ${o.velikost} kvadratnih metrov</p>
                            <p>Stevilo sostanovalcev: ${o.stsostanovalcev} </p>


                            <script>
                                var naslov = "${o.naziv}";
                            </script>



                            <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=maribor&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>Google Maps Generator by <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div>


                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                </div>




            </tr>
        </c:forEach>
    </table>



    <


</div>



<script>
    function myFunction() {
        document.getElementById("gmap_canvas").src = "https://maps.google.com/maps?q="+ naslov + "&t=&z=13&ie=UTF8&iwloc=&output=embed";
    }

    myFunction();
</script>



</body>
<footer>
    <div class="jumbotron text-center" style="margin-bottom:0">
    </div>
</footer>
</html>