<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>viewOthers</title>
</head>
<body style="background-color: #ccebff" onload="prijavljen()">
<c:if test="${prijavljen eq true}">


    <div class="row text-right" style="background-color:lightskyblue">
        <div class="col-9"></div>
        <div class="col-2 text-right">
            <p>Prijavljen uporabnik: ${ime}</p>
        </div>
        <div class="col-1 text-right">
            <form action="/odjava" method="post">

                <button type="submit">Odjava</button><br>

            </form>
        </div>
    </div>

</c:if>


<div class="jumbotron text-center" style="margin-bottom:0; background-color:lightskyblue">

    <h1>Cimri</h1>
    <p>Preprosto najemanje in oddajanje sob!</p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="#">Meni</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home">Domaca Stran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="view">Ogled Sob</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="dodajanjeNep">Dodajanje Sob</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container text-center" style="background-color: #b3e0ff">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <form method="post" action="/iskanjepoparametrih" enctype="multipart/form-data">

                <div class="dropdown">
                    Tip nepremicnine:<br>
                    <select id="tip" name="tipnepremicnine" >
                        <option value="1">Hisa</option>
                        <option value="2">Stanovanje</option>
                        <option value="3">Soba</option>
                        <option value="4">Kavc</option>
                    </select>



                </div>
                <div class="dropdown">
                    Regija:
                    <br>
                    <select id="regija" name="regija" >
                        <option value="1">Primorska</option>
                        <option value="2">Dolenjska</option>
                        <option value="3">Osrednje-slovenska</option>
                        <option value="4">Goriška</option>
                        <option value="5">Gorenjska</option>
                        <option value="6">Savinjska</option>
                        <option value="7">Podravska</option>
                        <option value="8">Pomurska</option>

                    </select>


                </div>

                <div class="form-group">
                    <label for="cenaOd">Cena od:</label>
                    <input type="text" class="form-control" id="cenaOd" name="cenaOd">
                </div>

                <div class="form-group">
                    <label for="cenaDo">Cena do:</label>
                    <input type="text" class="form-control" id="cenaDo" name="cenaDo">
                </div>



                <button type="submit" value="OK" class="btn btn-primary" style="margin-bottom:1%"><a  class="text-light">Iskanje</a></button>
            </form>
        </div>
        <div class="col-3"></div>
    </div>
</div>

</body>
<footer>
    <div class="jumbotron text-center" style="margin-bottom:0">
    </div>
</footer>
</html>