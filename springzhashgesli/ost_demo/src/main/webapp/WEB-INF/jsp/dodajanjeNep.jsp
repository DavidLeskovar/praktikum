<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>viewOthers</title>
</head>
<body style="background-color: #ccebff" onload="prijavljen()">

<c:if test="${prijavljen eq true}">


    <div class="row text-right" style="background-color:lightskyblue">
        <div class="col-9"></div>
        <div class="col-2 text-right">
            <p>Prijavljen uporabnik: ${ime}</p>
        </div>
        <div class="col-1 text-right">
            <form action="/odjava" method="post">

                <button type="submit">Odjava</button><br>

            </form>
        </div>
    </div>

</c:if>


<div class="jumbotron text-center" style="margin-bottom:0; background-color:lightskyblue">

    <h1>Cimri</h1>
    <p>Preprosto najemanje in oddajanje sob!</p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="#">Meni</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home">Domaca Stran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="view">Ogled Sob</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="dodajanjeNep">Dodajanje Sob</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container text-center" style="background-color: #b3e0ff">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <form method="post" action="/addNepremicnina" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="naziv">Naziv:</label>
                    <input type="text" class="form-control" id="naziv" name="name">
                </div>
                <div class="form-group">
                    <label for="velikost">Velikost:</label>
                    <input type="text" class="form-control" id="velikost" name="velikost">
                </div>
                <div class="form-group">
                    <label for="stSost">Stevilo sostanovalcev:</label>
                    <input type="text" class="form-control" id="stSost" name="stsostanovalcev">
                </div>
                <div class="form-group">
                    <label for="cena">Cena:</label>
                    <input type="text" class="form-control" id="cena" name="cenanajema">
                </div>
                <div class="form-group">
                    <label for="stroski">Mesecni stroski:</label>
                    <input type="text" class="form-control" id="stroski" name="cenamesstroskov">
                </div>
                <div class="form-group">
                    <label for="opis">Opis:</label>
                    <textarea type="text" class="form-control" id="opis" name="opis" rows="5"></textarea>
                </div>
                <div class="dropdown">
                    Tip nepremicnine:<br>
                    <select id="tip" name="tipnepremicnine" >
                        <option value="1">Hisa</option>
                        <option value="2">Stanovanje</option>
                        <option value="3">Soba</option>
                        <option value="4">Kavc</option>
                    </select>



                </div>
                <div class="dropdown">
                    Regija:
                    <br>
                    <select id="regija" name="regija" >
                        <option value="1">Primorska</option>
                        <option value="2">Dolenjska</option>
                        <option value="3">Osrednje-slovenska</option>
                        <option value="4">Goriska</option>
                        <option value="5">Gorenjska</option>
                        <option value="6">Savinjska</option>
                        <option value="7">Podravska</option>
                        <option value="8">Pomurska</option>

                    </select>


                </div>
                <div class="form-group">
                    <label for="drzava">Drzava:</label>
                    <input type="text" class="form-control" id="drzava" name="drzava">
                </div>
                <div class="form-group">
                    <label for="mesto">Mesto:</label>
                    <input type="text" class="form-control" id="mesto" name="mesto">
                </div>
                <div class="form-group">
                    <label for="okolica">Okolica:</label>
                    <input type="text" class="form-control" id="okolica" name="okolica">
                </div>
                <div class="dropdown">
                    Tip Lastnika:
                    <br>
                    <select id="tiplastnika" name="tiplastnika" >
                        <option value="1">Lastnik</option>
                        <option value="2">Najemnik</option>

                    </select>
                </div>
                <div class="form-group">
                    <label for="nsl">Naslov:</label>
                    <input type="text" class="form-control" id="nsl" name="naslov">
                </div>
                <div class="form-group">
                    <label for="slika">Slika:</label>
                    <input type="file" class="form-control" id="slika" name="slika">
                </div>


                <button type="submit" value="OK" class="btn btn-primary" style="margin-bottom:1%"><a  class="text-light">Dodaj</a></button>
            </form>
        </div>
        <div class="col-3"></div>
    </div>
</div>

</body>
<footer>
    <div class="jumbotron text-center" style="margin-bottom:0">
    </div>
</footer>
</html>