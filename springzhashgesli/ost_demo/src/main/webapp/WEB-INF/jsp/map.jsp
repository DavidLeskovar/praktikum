<!DOCTYPE html>
<html>
<head>
    <title>Geocoding service</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
</head>
<body>
<div id="map"></div>
<script type="text/javascript">

    var geocoder = new google.maps.Geocoder();
    var address = "Maribor";

    geocoder.geocode( { 'address': address}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();


            initialize(latitude,longitude);

        }

    });


    function initialize(latitude,longitude) {
        var latlng = new google.maps.LatLng(latitude,longitude);

        var myOptions = {
            zoom: 14,
            center: latlng

        };
        var map = new google.maps.Map(document.getElementById("map"),myOptions);

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
        });
    }


</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYvmaWOABtUYuV82R5cHqly-ZSUksBEHA&callback=initMap"
        type="text/javascript"></script>
</body>
</html>