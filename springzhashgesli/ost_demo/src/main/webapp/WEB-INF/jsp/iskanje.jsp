<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>home</title>
</head>
<body style="background-color: #ccebff" onload="prijavljen()">
<script>
    function prijavljen(){
        var pri = ${trenutni};
        if (pri!=null){
            document.getElementById("prij").innerHTML = ${trenutni}
        }
    }
</script>
<div class="jumbotron text-center" style="margin-bottom:0; background-color:lightskyblue">
    <p id="prij" style="position: absolute; top: 8px; right: 8px">h</p>
    <h1>Cimri</h1>
    <p>Preprosto najemanje in oddajanje sob!</p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="#">Meni</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home">Domaca Stran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="view">Ogled Sob</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="dodajanjeNep">Dodajanje Sob</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container text-center" style="background-color: #b3e0ff">
    <div class="row">
        <div class="col-12 text-center">
            <form action="/iskanjepoparametrih" method="post">



                <div class="dropdown">
                    Tip nepremicnine:<br>
                    <select id="tip" name="tip" >
                        <option value="www">Hisa</option>
                        <option value="2">Stanovanje</option>
                        <option value="3">Soba</option>
                        <option value="4">Kavc</option>
                    </select>
                </div>

                <div class="dropdown">
                    <label for="regija">Regija:</label><br>
                    <button class="btn btn-light dropdown-toggle" type="button" id="regija" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" name="regija" style="margin-bottom:2%">
                        Dropdown
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu text-center" aria-labelledby="dropdownMenu2">
                        <li value="1"><a href="#">Primorska</a></li>
                        <li value="2"><a href="#">Dolenjska</a></li>
                        <li value="3"><a href="#">Osrednje-slovenska</a></li>
                        <li value="4"><a href="#">Goriska</a></li>
                        <li value="5"><a href="#">Gorenjska</a></li>
                        <li value="6"><a href="#">Savinjska</a></li>
                        <li value="7"><a href="#">Podravska</a></li>
                        <li value="8"><a href="#">Pomurska</a></li>
                    </ul>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-5"></div>
        <div class="col-2 text-center">
            <form action="/iskanjepoparametrih" method="post">
                <p>Cena:</p>
                    <div class="form-group">
                        <label for="cenaMin">Od:</label>
                        <input type="text" class="form-control" id="cenaMin" name="cenaOd">
                    </div>
                    <div class="form-group">
                        <label for="cenaMax">Do:</label>
                        <input type="text" class="form-control" id="cenaMax" name="cenaDo">
                    </div>
                <button type="submit" value="OK" class="btn btn-primary" style="margin-bottom:1%"><a  class="text-light">Iskanje</a></button>
            </form>
        </div>
        <div class="col-5"></div>
    </div>
</div>
</body>

<script>
    $(".dropdown-menu li a").click(function(){
        $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
        $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
    });
</script>

<footer>
    <div class="jumbotron text-center" style="margin-bottom:0">
    </div>
</footer>
</html>