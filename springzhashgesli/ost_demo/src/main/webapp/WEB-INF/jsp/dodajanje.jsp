<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>register</title>
</head>
<body style="background-color: #ccebff" onload="prijavljen()">



<div class="jumbotron text-center" style="margin-bottom:0; background-color:lightskyblue">
    <h1>Cimri</h1>
    <p>Preprosto najemanje in oddajanje sob!</p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="#">Meni</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home">Domaca Stran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="view">Ogled Sob</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="dodajanjeNep">Dodajanje Sob</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container text-center" style="background-color: #b3e0ff">
    <h2>Registracija</h2>
    <form method="post" action="/addOseba" class="needs-validation" novalidate>
        <div class="form-group">
            <label for="name">Ime:</label>
            <input type="text" class="form-control" id="name" placeholder="Vnesite ime" name="name" required>
            <div class="valid-feedback">Veljavno.</div>
            <div class="invalid-feedback">Prosimo da izpolnite to polje.</div>
        </div>
        <div class="form-group">
            <label for="priim">Priimek:</label>
            <input type="text" class="form-control" id="priim" placeholder="Vnesite priimek" name="lastname" required>
            <div class="valid-feedback">Veljavno.</div>
            <div class="invalid-feedback">Prosimo da izpolnite to polje.</div>
        </div>
        <div class="form-group">
            <label for="uname">Uporabnisko Ime:</label>
            <input type="text" class="form-control" id="uname" placeholder="Vnesite uporabnisko ime" name="username" required>
            <div class="valid-feedback">Veljavno.</div>
            <div class="invalid-feedback">Prosimo da izpolnite to polje.</div>
        </div>
        <div class="form-group">
            <label for="pwd">Geslo:</label>
            <input type="password" class="form-control" id="pwd" placeholder="Vnesite geslo" name="password" required>
            <div class="valid-feedback">Veljavno.</div>
            <div class="invalid-feedback">Prosimo da izpolnite to polje.</div>
        </div>
        <div class="form-group">
            <label for="email">E-postni Naslov:</label>
            <input type="text" class="form-control" id="email" placeholder="Vnesite E-postni naslov" name="ePostniNaslov" required>
            <div class="valid-feedback">Veljavno.</div>
            <div class="invalid-feedback">Prosimo da izpolnite to polje.</div>
        </div>

        <div class="form-group form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="remember"> Zapomni si me.
            </label>
        </div>
        <button type="submit" value="OK" class="btn btn-primary" style="margin-bottom:1%">Registracija</button>
    </form>
</div>

</body>
<footer>
    <div class="jumbotron text-center" style="margin-bottom:0">
    </div>
</footer>
</html>