<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>view</title>
</head>
<body style="background-color: #ccebff" onload="prijavljen()">


<c:if test="${prijavljen eq true}">


    <div class="row text-right" style="background-color:lightskyblue">
        <div class="col-9"></div>
        <div class="col-2 text-right">
            <p>Prijavljen uporabnik: ${ime}</p>
        </div>
        <div class="col-1 text-right">
            <form action="/odjava" method="post">

                <button type="submit">Odjava</button><br>

            </form>
        </div>
    </div>

</c:if>



<div class="jumbotron text-center" style="margin-bottom:0; background-color:lightskyblue">
    <h1>Cimri</h1>
    <p>Preprosto najemanje in oddajanje sob!</p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="#">Meni</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home">Domaca Stran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="view">Ogled Sob</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="dodajanjeNep">Dodajanje Sob</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container text-center" style="background-color: #b3e0ff">
    <div class="row">
        <div class="col-12">
            <p>Ali zelite pregledati svoje sobe, ali iskati sobe drugih?</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-4" style="background-color: #ffc180; margin:1%">
            <h3>Pregled ponujenih sob</h3>
            <p>Tukaj so navedene vse sobe, ki jih vi ponujate drugim.</p>
            <button type="button" class="btn btn-light" style="margin-bottom:1%"><a href="viewOwn">Moje sobe</a></button>
        </div>
        <div class="col-sm-4" style="background-color: #ffc180; margin:1%">
            <h3>Iskanje sob</h3>
            <p>Tukaj lahko izbirate med sobami, ki jih ponujajo drugi.</p>
            <button type="button" class="btn btn-light" style="margin-bottom:1%"><a href="iskanje">Iskanje sob</a></button>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>

</body>
<footer>
    <div class="jumbotron text-center" style="margin-bottom:0">
    </div>
</footer>
</html>